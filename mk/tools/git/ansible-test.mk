ansible-test_NAME         = Ansible Test
ansible-test_RELEASE      ?= $(ansible_RELEASE)
ansible-test_DOWNLOAD_URL ?= $(ansible_DOWNLOAD_URL)
ansible-test_DEPENDENCIES ?= $(ansible_DEPENDENCIES)
ansible-test_BIN_DIR      ?= $(ansible_BIN_DIR)
ansible-test_PARENT       = ansible

# vi:syntax=makefile

ansible-connection_NAME         = Ansible Connection
ansible-connection_RELEASE      ?= $(ansible_RELEASE)
ansible-connection_DOWNLOAD_URL ?= $(ansible_DOWNLOAD_URL)
ansible-connection_DEPENDENCIES ?= $(ansible_DEPENDENCIES)
ansible-connection_BIN_DIR      ?= $(ansible_BIN_DIR)
ansible-connection_PARENT       = ansible

# vi:syntax=makefile

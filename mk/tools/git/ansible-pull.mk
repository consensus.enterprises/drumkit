ansible-pull_NAME         = Ansible Pull
ansible-pull_RELEASE      ?= $(ansible_RELEASE)
ansible-pull_DOWNLOAD_URL ?= $(ansible_DOWNLOAD_URL)
ansible-pull_DEPENDENCIES ?= $(ansible_DEPENDENCIES)
ansible-pull_BIN_DIR      ?= $(ansible_BIN_DIR)
ansible-pull_PARENT       = ansible

# vi:syntax=makefile

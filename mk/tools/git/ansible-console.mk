ansible-console_NAME         = Ansible Console
ansible-console_RELEASE      ?= $(ansible_RELEASE)
ansible-console_DOWNLOAD_URL ?= $(ansible_DOWNLOAD_URL)
ansible-console_DEPENDENCIES ?= $(ansible_DEPENDENCIES)
ansible-console_BIN_DIR      ?= $(ansible_BIN_DIR)
ansible-console_PARENT       = ansible

# vi:syntax=makefile

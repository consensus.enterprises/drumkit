ansible-vault_NAME         = Ansible Vault
ansible-vault_RELEASE      ?= $(ansible_RELEASE)
ansible-vault_DOWNLOAD_URL ?= $(ansible_DOWNLOAD_URL)
ansible-vault_DEPENDENCIES ?= $(ansible_DEPENDENCIES)
ansible-vault_BIN_DIR      ?= $(ansible_BIN_DIR)
ansible-vault_PARENT       = ansible

# vi:syntax=makefile

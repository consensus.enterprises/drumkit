ansible-doc_NAME         = Ansible Doc
ansible-doc_RELEASE      ?= $(ansible_RELEASE)
ansible-doc_DOWNLOAD_URL ?= $(ansible_DOWNLOAD_URL)
ansible-doc_DEPENDENCIES ?= $(ansible_DEPENDENCIES)
ansible-doc_BIN_DIR      ?= $(ansible_BIN_DIR)
ansible-doc_PARENT       = ansible

# vi:syntax=makefile

ansible-config_NAME         = Ansible Config
ansible-config_RELEASE      ?= $(ansible_RELEASE)
ansible-config_DOWNLOAD_URL ?= $(ansible_DOWNLOAD_URL)
ansible-config_DEPENDENCIES ?= $(ansible_DEPENDENCIES)
ansible-config_BIN_DIR      ?= $(ansible_BIN_DIR)
ansible-config_PARENT       = ansible

# vi:syntax=makefile

cluster_name         = "{{ CLUSTER_NAME }}"
cluster_keypair      = "{{ CLUSTER_NAME_LC }}-cluster-keypair"
cluster_master_count = 2
cluster_node_count   = 2

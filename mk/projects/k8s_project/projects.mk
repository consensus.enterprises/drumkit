BRANCH_NAME := $(shell git rev-parse --abbrev-ref HEAD)

.k8s-init-branch-environment:
	$(ECHO) "Setting up an environment for branch $(BRANCH_NAME)."
	@$(make) init-k8s-environment \
            K8S_ENVIRONMENT_NAME=$(BRANCH_NAME)
	@$(make) init-k8s-drupal-app \
            K8S_DRUPAL_APP_IMAGE_TAG=$(BRANCH_NAME) \
            K8S_ENVIRONMENT_NAME=$(BRANCH_NAME)

.new-branch-environment-intro:
	$(ECHO) "You are about to create a new branch environment for: '$(BRANCH_NAME)'."

.new-branch-environment: .new-branch-environment-intro
.new-branch-environment: .confirm-proceed
.new-branch-environment: .k8s-init-branch-environment
.new-branch-environment: # Create a new environment for the current branch.
	$(make) $(BRANCH_NAME)-create-environment
	$(make) build-branch-image CONFIRM=y
	$(make) $(BRANCH_NAME)-deploy-app

.refresh-branch-environment-intro:
	$(ECHO) "You are about to update the '$(BRANCH_NAME)' environment with the latest changes in the branch."

.refresh-branch-environment: .refresh-branch-environment-intro
.refresh-branch-environment: .confirm-proceed
.refresh-branch-environment: # Update the environment for the current branch with the latest changes.
	$(make) build-branch-image CONFIRM=y
	$(make) .redeploy-drupal-app \
            DRUPAL_APP_ENVIRONMENT=$(BRANCH_NAME) \
            CONFIRM=y

# TODO: move .re-create to its own task, add tests:
.re-create:
	rm $(FILENAME)
	$(make) $(FILENAME)

.build-branch-image-intro:
	$(ECHO) "You are about to update the Docker image for the '$(BRANCH_NAME)' branch."

.build-branch-image: .build-branch-image-intro
.build-branch-image: .confirm-proceed
.build-branch-image: ## Build a test Drupal Docker image using the current branch.
	@DOCKER_IMAGE_TAG=$(BRANCH_NAME) $(make) docker-image-drupal

.k8s-clean-branch-environment:
	@$(make) clean-k8s-environment \
            K8S_ENVIRONMENT_NAME=$(BRANCH_NAME)
	@$(make) clean-k8s-drupal-app \
            K8S_DRUPAL_APP_IMAGE_TAG=$(BRANCH_NAME) \
            K8S_ENVIRONMENT_NAME=$(BRANCH_NAME)

.clean-branch-environment-intro:
	$(ECHO) "You are about to delete the branch environment for: '$(BRANCH_NAME)'."

.clean-branch-environment: .clean-branch-environment-intro
.clean-branch-environment: .confirm-proceed
.clean-branch-environment: # Delete environment and app config for the current branch.
#	$(make) $(BRANCH_NAME)-delete-app
#	$(make) $(BRANCH_NAME)-delete-environment
	$(make) .k8s-clean-branch-environment

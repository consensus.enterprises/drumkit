K8S_PROJECT_SCRIPT_DIR := $(dir $(lastword $(MAKEFILE_LIST)))scripts

.wait-for-drupal-pod: # Return when the Drupal pod in question is in Running state.
	@$(K8S_PROJECT_SCRIPT_DIR)/wait-for-drupal-pod.sh

# @TODO: Figure out how to make this work with multiple application pods.
.run-command-on-drupal-pod: .wait-for-drupal-pod
.run-command-on-drupal-pod: # Run a command on the Drupal container.
	@kubectl exec -it `$(K8S_PROJECT_SCRIPT_DIR)/get-running-drupal-pod-id.sh` -- $(REMOTE_COMMAND)

# Deleting the application deployment is needed, for now, because only one pod can
# connect to our PVCs. So, during a rollout, a new pod will fail, because the
# existing pod is attached to the PVC. Once we have `csi-manila-cephfs`-backed
# PVCs, we should be able to do a rollout for (multiple) application pods.
.redeploy-drupal-app: .redeploy-drupal-app-intro
.redeploy-drupal-app: .confirm-proceed
.redeploy-drupal-app-intro: 
	@$(ECHO) "$(YELLOW)==> You are about to redeploy the $(DRUPAL_APP_ENVIRONMENT) environment.$(RESET)"
	@$(ECHO) "$(ORANGE)==> Did you remember to take a backup?$(RESET)"

.redeploy-drupal-app:
	@$(ECHO) "$(YELLOW)==> Switching to $(DRUPAL_APP_ENVIRONMENT) environment.$(RESET)"
	$(make) $(DRUPAL_APP_ENVIRONMENT)-use-environment
	@$(ECHO) "$(YELLOW)==> Deleting Drupal deployment.$(RESET)"
	$(kubectl) delete deployment/drupal-deployment
	@$(ECHO) "$(YELLOW)==> Re-deploying Drupal component.$(RESET)"
	$(make) $(DRUPAL_APP_ENVIRONMENT)-deploy-app
	@sleep 1
	$(make) .run-drupal-database-updates

.run-drupal-database-updates:
	@$(ECHO) "$(YELLOW)==> Running Drupal database updates.$(RESET)"
	@$(make) run-command-on-drupal-pod REMOTE_COMMAND="bin/drush updatedb:status"
	@echo -n "Proceed with database updates? [y/N] " && read ans && [ $${ans:-N} = y ]
	@$(make) run-command-on-drupal-pod REMOTE_COMMAND="bin/drush updatedb"
	@$(make) run-command-on-drupal-pod REMOTE_COMMAND="bin/drush status"


